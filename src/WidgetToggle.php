<?php

namespace Drupal\field_widget_toggle;

class WidgetToggle {

  /**
   * widget types that field widget toggle works with
   *
   * ToDo: to be extended for other types
   */
  public const SUPPORTED_WIDGET_TYPES = [
    'entity_reference_autocomplete',
  ];
}