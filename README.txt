Field Widget Toggle

Toggle the body of multivalued fields that use the Entity Reference Autocomplete Widget (entity_reference_autocomplete).

This is pure UX improvement, especially if you have lots of values in the field.
If you click the header of the field the visibility of the body (aka the values) will be toggled.

Currently only entity_reference_autocomplete field widget is targeted, as this was my current need.

Maybe this can be extended for other widgets later (feature requests as well as patches are welcome).

Installation

Install module as usual

Configure field widget settings on form display to use the toggle switch for your fields (only entity_reference_autocomplete at the moment)
