(function ($, Drupal) {
  Drupal.behaviors.fieldWidgetToggle = {
    attach: function attach(context) {

      //let toggles = document.querySelectorAll('.field-toggle');
      let toggles = document.querySelectorAll('.field-widget-toggle');
      let closed_toggles = document.querySelectorAll('.field-widget-toggle.closed');

      for (const toggle of toggles) {
        // Letting this in, only to showcase variable passing
        let toggle_source = toggle;
        addClickHandler(toggle_source, toggle);
      }

      for (const closed_toggle of closed_toggles) {
        closed_toggle.parentNode.classList.toggle('toggle-hide');
      }

      function addClickHandler(elem, toggle) {
        elem.addEventListener('click', function (e) {
          toggle.parentNode.classList.toggle('toggle-hide');
        });
      }
    }
  };
})(jQuery, Drupal);